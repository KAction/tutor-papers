import useful;
import geometry;
import graph;
size(10cm);

deftriangle("ABC", triangle(radians(40), radians(70)));
defpoint("D", intersectionpoint(line(B, C), line(A, unit(B -A) + unit(C - A))), S);
draw(A -- D);
markangle(B, A, D, radius = 0.9markangleradius);
markangle(D, A, C);
defpoint("K", (A + D)/2, W);
line KM = perpendicular(K, line(A,D));
defpoint("M", intersectionpoint(KM, line(A,C)), E);
draw(K -- M);
markrightangle(M, K, A);
draw(M -- D);
draw(segment(D, A), marker = StickIntervalMarker(n = 2));
markangle(M, D, A);
