import useful;
import geometry;
import graph;
size(10cm);

real alpha = 124;
real beta = 116;

deftriangle("MBC", triangle(radians(60), radians(60)));
point Ax = B + rotate(alpha) * (C - B);
point Dx = C + rotate(-beta) * (B - C);

line lAD = line(M, M + rotate(240 - 2 * beta) * (C - B));
point A = intersectionpoint(lAD, line(B, Ax));
point D = intersectionpoint(lAD, line(C, Dx));
draw(A -- D -- C -- B -- cycle);
label("$A$", A, W);
label("$D$", D, E);

draw(segment(A, M), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(B, M), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(C, M), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(D, M), marker = StickIntervalMarker(n = 2, i = 1));

markangle("$\alpha$", B, A, M, n = 2, radius = markangleradius/1.5);
markangle("$\alpha$", M, B, A, n = 2, radius = markangleradius/1.5);

markangle("$\gamma$", M, C, B, n = 1, radius = markangleradius/1.5);
markangle("$\gamma$", C, B, M, n = 1, radius = markangleradius/1.5);

markangle("$\beta$", D, C, M, n = 3, radius = markangleradius/1.5);
markangle("$\beta$", M, D, C, n = 3, radius = markangleradius/1.5);

label("6", B -- C);
