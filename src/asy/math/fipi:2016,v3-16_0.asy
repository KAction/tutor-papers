import useful;
size(10cm);


real r = 2.5;
real R = 8.70;
path c1 = circle((0, 0), r);
path c2 = circle((r + R, 0), R);

pair a = (-r, 0);
pair b = (r, 0);
real phi = acos(1/5);
pair k = (r * cos(phi), r * sin(phi));
pair c = (r + 2 * R, 0);
pair m = point(c2, intersections(c2, a, k)[0]);
pair d = point(c1, intersections(c1, m, b)[1]);

draw(c1);
draw(c2);
draw(a -- k -- b -- a -- c -- m -- d -- a -- m -- cycle);

label("$A$", a, W);
label("$B$", b, SE);
label("$C$", c, E);
label("$K$", k, N);
label("$M$", m, N);
label("$D$", d, S);
label("$12$", (k + m)/ 2, N);
label("$3$", (2a + k)/ 3, N);
draw(orth(m, b, c, scale = 0.1));
draw(orth(d, a, b));



