import useful;
import geometry;
import graph;
size(10cm);

point point(real alpha) {
  return (cos(alpha), sin(alpha));
}

defpoint("A", point(pi/2), N);
defpoint("B", point(2pi/3), NW);
defpoint("C", point(11pi/6), E);
fill(shift(A) * scale(0.015) * unitcircle);
fill(shift(B) * scale(0.015) * unitcircle);
fill(shift(C) * scale(0.015) * unitcircle);
draw(arc((0,0), 1, degrees(pi/2), degrees(2pi/3)), linewidth(2));
draw(arc((0,0), 1, degrees(2pi/3), degrees(11pi/6)), linewidth(0.4) + dashed);
draw(arc((0,0), 1, degrees(-pi/6), degrees(pi/2)), linewidth(3) + dotted);





// draw(unitcircle);
