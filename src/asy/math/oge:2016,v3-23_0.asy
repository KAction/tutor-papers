import useful;
import graph;

import graph;
size(10cm);

xaxis("$x$", LeftTicks(Step = 1), EndArrow);
yaxis("$y$", LeftTicks(Step = 1), EndArrow);

real f (real x) { return -x*x - 4 * x + 1; }
real g (real x) { return -x + 1; }
typedef real realfunc(real);
realfunc constant(real c) {
  return new real (real _) { return c; };
};


draw(graph(f, -3, 0.9));
draw(graph(f, -4.8, -3), dotted);
draw(graph(g, -4.8, -3));
draw(graph(g, -3, 0.9), dotted);
draw(graph(constant(5), -4.8, 0.9), dashed);
draw(graph(constant(4), -4.8, 0.9), dashed);
