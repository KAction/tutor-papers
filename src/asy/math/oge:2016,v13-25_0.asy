import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

quadrilateral tr = parallelogram(pi/3, 1.6);
unravel tr;
draw(tr);
defpoint("E", (A + B + D)/3, 2W + N);
draw(B -- E -- C -- cycle);
draw(A -- E -- D -- cycle);
defaltitude("P", triangle(B, E, C), 2);
defaltitude("Q", triangle(A,E, D), 2);
