import useful;
import geometry;

size(10cm);
pair B = (0, 0);
real phi = pi/6;
pair D = (cos(phi), sin(phi));
pair D1 = (cos(phi), 0);
pair A = 3D;
pair A1 = 3D1;
label("$B$", B, S);
label("$D$", D, NW + W);
label("$A$", A, N);
draw(3.5D -- B -- (4, 0));
label("$C$", (3.5, 0), S);
pair F2 = (-sqrt(3), 0);
label("$F_2$", F2, SW);
draw(B -- F2, dashed);
pair P = (-sqrt(3), -1);
label("$P$", P, S);
draw(B -- P, dashed);
circle circ = circle(A, D, F2);
draw(circ);
pair Q = (-sqrt(3), 14);
label("$Q$", Q, N);
draw(P -- Q);
draw(orth(F2, B, Q));

