import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;

size(10cm);

real f (real x) { return (x + 9) ^2; }

typedef real realfn(real x);
realfn derivate(realfn f) {
  return new real (real x) {
    real h = 0.0001;
    return (f(x + h) - f(x - h))/2h;
  };
};

realfn tangent(realfn f, real x0) {
  var k = derivate(f)(x0);
  var y0 = f(x0);
  return new real(real x) {
    return y0 + k * (x - x0);
  };
}

real newton(realfn f, real x0) {
  var deriv = derivate(f);
  while (abs(f(x0)) > 0.001) {
    write(x0);
    x0 -= f(x0)/deriv(x0);
  };
  return x0;
}


real K = 9;
xaxis("$x$", RightTicks(Step = 1), EndArrow);
yaxis("$y$", RightTicks(Step = 9), EndArrow);
scale(Linear(1), Linear(1/K));

draw(graph(f, -9, 0), linewidth(1.1));

var tang = tangent(f, -3);
real L = newton(tang, -3);
draw(graph(tang, L, 0));
for (real x = L; x <= 0; x += 0.08) {
  draw((x, 0) -- (x, tang(x)/K), dotted);
}
