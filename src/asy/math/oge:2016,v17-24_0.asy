import useful;
import geometry;
import graph;
size(10cm);

real a = 28;
real b = 21;

triangle t = triangle(atan(b/a), pi/2);
deftriangle("ABC", t);
markrightangle(A, C, B);

defaltitude("H", t, 3);
label(string(b), A -- C);
label(string(a), B -- C);
