import geometry_quadrilateral;
import useful;
import geometry;
import graph;

size(10cm);

point midpoint(point p1, point p2) {
  return (p1 + p2)/2;
};


quadrilateral ABCD = rectangle((0,0), (1.6, 1));
unravel ABCD;
defpoint("M", midpoint(A, B), W);
defpoint("N", midpoint(B, C), N);
defpoint("K", midpoint(C, D), E);
defpoint("R", midpoint(A, D), S);
draw(M -- N -- K -- R --cycle);
draw(segment(A, B), marker = StickIntervalMarker(n = 2));
draw(segment(C, D), marker = StickIntervalMarker(n = 2));

draw(segment(B, C), marker = StickIntervalMarker(n = 1));
draw(segment(A, D), marker = StickIntervalMarker(n = 1));
markrightangle(A, B, C);
markrightangle(B, C, D);
markrightangle(C, D, A);
markrightangle(D, A, B);

draw(ABCD);
quadrilateral ABCD = rectangle(midpoint(M, R), midpoint(N, K));
draw(ABCD, labels ="");
unravel ABCD;
label("$A_1$", A, unit(A - C));
label("$B_1$", B, unit(B - D));
label("$C_1$", C, unit(C - A));
label("$D_1$", D, unit(D - B));
draw(segment(M, N), marker = StickIntervalMarker(n = 3));
draw(segment(N, K), marker = StickIntervalMarker(n = 3));
draw(segment(K, R), marker = StickIntervalMarker(n = 3));
draw(segment(R, M), marker = StickIntervalMarker(n = 3));
draw(A -- C, dashed + linewidth(1.1));
//draw(B -- D, dashed + linewidth(1.1));
