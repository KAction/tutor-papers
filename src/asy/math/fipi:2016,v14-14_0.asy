import solids;
import useful;
import graph3;

settings.render=0;
settings.prc=false;

size(200);

revolution r=sphere(O, 1);
draw(r,1,longitudinalpen=nullpen);
draw(r.silhouette());

real x(real t) {
  return 0.6 * sin(t);
}
real y(real t) {
  return 0.6 * cos(t);
}
typedef real realfunc(real);
realfunc const(real x) {
  return new real (real _val) { return x; };
}

draw(graph(x, y, const(0.8), 0, 2pi, operator..), dotted);
draw(graph(
  new real(real t) { return 0.8 sin(t); },
  new real(real t) { return 0.8 cos(t); },
  const(-0.6), 0, 2pi, operator..), dotted);
real t = 2.5;
triple C = (0, 0, 0.8);
triple B = (0.6 sin(t), 0.6 cos(t), 0.8);
label("$O$", O, W);
label("$C$", C, E);
label("$B$", B, W);
draw(C -- O -- B -- cycle);
draw(orth(C, O, B, scale = 0.15));
