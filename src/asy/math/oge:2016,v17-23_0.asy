import useful;
import geometry;
import graph;
size(10cm);

xaxis("$x$", RightTicks(Step = 1), EndArrow);
yaxis("$y$", RightTicks(Step = 3), EndArrow);

real xl = -7;
real xr = 8;

draw(graph(new real(real x) { return x^2 - 7x; }, 0, xr));
draw(graph(new real(real x) { return -x^2 - 5x; }, xl, 0));
path p = (xl, -12.25) -- (xr, -12.25);
draw(p, linewidth(1.2));
label("$y = -12.25$", (xl + 3, -12.25), S);

path p = (xr, 6.25) -- (xl, 6.25);
draw(p, linewidth(1.2));
label("$y = 6.25$", (xr - 2, 6.25), N);
