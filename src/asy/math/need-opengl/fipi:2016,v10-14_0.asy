import three;
import useful;
size3(10cm);

real R = sqrt(3);
triple A = R * (sin(2pi/3), cos(2pi/3), 0);
triple B = (0, R, 0);
triple C = R * (sin(4pi/3), cos(4pi/3), 0);
triple h = (0, 0, 1);
triple A1 = A + h;
triple B1 = B + h;
triple C1 = C + h;


draw(A -- B -- C -- cycle, dotted);
draw(A1 -- B1 -- C1 -- cycle);
draw(A -- A1, dotted);
draw(B -- B1, dotted);
draw(C -- C1, dotted);
draw(A1 -- B1 -- B -- A -- cycle);
draw(B1 -- A);

label("$A$", A, E);
label("$B$", B, NE);
label("$C$", C, 2NE + E);
label("$A_1$", A1, SW);
label("$B_1$", B1, NW);
label("$C_1$", C1, W);

triple D = (C + C1)/2;
label("$D$", D, NW);

draw(A -- B1 -- D -- cycle, dashed);

triple R = B + 2 (C - B);
draw(C -- R, dotted);
draw((C + 0.1(R -C)) -- R);
draw(D -- R, dashed);
draw((D + 0.05(R -D)) -- R);
label("$R$", R, SE);
draw(A -- R);
// It is magic. And try-and-hit aganist wall.
currentprojection = oblique(150);
draw(orth(B, A, B1));
draw(orth(A, R, B1, scale = 0.09));
draw(angle(A, B1, B, scale=  0.3));


