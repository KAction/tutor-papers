import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (0, 0), W);
defpoint("B", (10, 13), NW);
defpoint("D", (20, -15), S);
draw(A -- B -- D -- cycle);
circle c = circle(A, B, D);
real r = 0.5;
defpoint("K", (r * B + D)/(r + 1), SW);
draw(c);
point C = intersectionpoints(c, line(A, K))[0];
label("$C$", C, E);
draw(A -- B -- C -- D -- cycle);
draw(A -- C);
path a = angle(K, C, B);
draw(a);
usepackage("gensymb");
label("$120\degree$", a);

path a = angle(C, B, K);
draw(a);
label("$\alpha$", a);

path a = angle(B, K, C);
draw(a);
label("$\beta$", a);

label("25", A -- B);
label("16", C -- D);
