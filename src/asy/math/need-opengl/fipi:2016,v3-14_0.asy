import three;
import useful;
size3(10cm);

triple s = (0, 0, 5);
triple a = (0, 0, 0);
triple b = (0, sqrt(11), 0);
triple c = (2 * sqrt(3), sqrt(11), 0);
triple d = (2 * sqrt(3), 0, 0);
pen bold = linewidth(2);

label("$A$", a, W);
label("$B$", b, E + N);
label("$C$", c, E);
label("$D$", d, S);
label("$S$", s, N);

currentprojection = perspective(6, -2, 3);
// Use cycle as much as possible, it looks nicer
draw(a -- b -- c -- d -- cycle, dashed);
draw(a -- d -- c);
draw(s -- a -- d -- s -- c -- d -- cycle);
draw(s -- c, bold);
draw(s -- b, bold + dashed);

path3 phi = angle(s, b, c);
draw(phi);
label("$\varphi$", phi);

draw(orth(a, s, b, scale = 0.1), dashed);
draw(orth(a, s, d, scale = 0.1));
draw(orth(b, s, c), dashed);

