import useful;
import geometry;
import _stereo;
size3(10cm);

var P = parallelepiped(7, 5, 3);
label(P);
Eye.add_object(P);
unravel P;
var L = (2A + 3D)/5;
var K = (3A1 + 4B1)/7;
var M = (3B1 + 2C1)/5;
label("$L$", L, -Z);
label("$M$", M, +Z);
draw(A -- M -- C1 -- L -- cycle, dashed);
draw(A -- B1);
draw(D -- C1, dashed);
label("K", K, Z);
var l1 = length(A - A1);
var l2 = length(A1 - B1);
l1 = l1 * l1; l2 = l2 * l2;
var T = A + l1/(l1 + l2) *(B1 -A);
draw(A1 -- T);
label("$T$", T, -Z);
var H = B1 + 3/7 * (T - B1);
draw(K -- H);
label("$H$", H, -Z);

draw(orth(T, A1, B1, scale = 0.07));
draw(orth(H, K, B1, scale = 0.1));


var camera = (17, 20, 5);
currentprojection = perspective(camera);
Eye.draw();
