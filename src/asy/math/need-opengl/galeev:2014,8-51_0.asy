import useful;
import _stereo;
import geometry;

size(10cm);

var base = triangleAbc(60, 14, 8);
base = triangle(base.C, base.B, base.A);
var py = pyramid3(base, 4sqrt(3), base.A);
label(py);
unravel py;
var D = S;
draw(D -- C);
triple T = (B + D)/2;
triple V = (D + C)/2;
triple R = (A + C)/2;
triple P = (A + B)/2;
label("$T$", T, 2X + Z);
label("$V$", V, 2Y + Z);
label("$R$", R, 2X - Z);
label("$P$", P, 2X - Z);
pair demote(triple X) { return (X.x, X.y); }
triple promote(pair X) { return (X.x, X.y, -2sqrt(3)); }

var alt = altitude(triangle(demote(C), demote(R), demote(P)).vertex(1));
var H = promote(alt.B);
label("$H$", H, -3Z);

Eye.add_object(py);
Eye.add_path(T -- V -- R -- P -- cycle);
draw(T -- V);
Eye.add_path(angle(C, A, B));
Eye.add_path(promote(alt.A) -- promote(alt.B));
Eye.add_path(H -- R);
Eye.add_path(orth(H, C, R));
Eye.add_path(angle(R, C, H, scale = 0.25));


var camera = (15, -12, 8);
currentprojection = perspective(camera);
Eye.draw(camera);
