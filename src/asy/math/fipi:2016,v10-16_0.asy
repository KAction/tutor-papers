import geometry;
size(10cm);

pair B = (0, 0);
pair D = (12, 0);
pair C = (7, 0);
pair A = (3.5, 12);
triangle abc = triangle(A, B, C);

point L = intersectionpoint(bisector(abc.vertex(2)), line(A, C));
triangle bld = triangle(B, L, D);

draw(abc);
draw(bld);
label("$A$", A, W);
label("$B$", B, W);
label("$C$", C, S);
label("$D$", D, E);
label("$L$", L, NE);

markangle("$\psi$",  line(B, L), line(B, A));
markangle("$\psi$",  line(B, C), line(B, L));
markangle("$\psi$",  line(D, L), line(D, B));
markangle("$\psi$",  line(L, C), line(L, D));
markangle(n = 2, "$2\psi$", line(C, A), line(C, B));

point Q = intersectionpoint(line(A, B), line(L, D));
label("$Q$", Q, NW + 0.5W);
triangle bqd = triangle(B, Q, D);
draw(bqd);

line alt = altitude(bqd.vertex(2));
point R = intersectionpoint(alt, line(B, D));
draw(triangle(B, Q, R));
label("$R$", R, S);
perpendicularmark(line(R, D), line(R, Q));

