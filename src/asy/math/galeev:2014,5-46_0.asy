import useful;
import graph;
size(10cm);
draw((-2.5, 0) -- (1.5, 0), EndArrow);
label("u", (1.5, 0), 1.5S);

draw((-2, 0) -- (-2, 0.2) -- (1, 0.2) -- (1, 0));
draw((-2,0) -- (0, 0), linewidth(2));
draw((1/2,0) -- (1, 0), linewidth(2));

mark_point(1, exclude = true);
draw(graph(new real(real x) { return - 0.07 * abs(sin(15(x - 0.5))); }, 0.5, 1.3, n = 1200));
mark_point(0.5, l = "$\displaystyle\frac 12$", exclude = true);

mark_point(-2, l = "$-10$", exclude = true);
draw(graph(new real(real x) { return - 0.07 * abs(sin(15x)); }, -2.4, 0, n = 1200));
mark_point(0, l = "$\displaystyle\frac 17$", exclude = true);
