import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (-2, -2), S);
defpoint("C", (2, 2), N);
defpoint("B", (-0.7, 2), W);
defpoint("D", (0.7, -2), E);
draw(segment(A, C), marker = StickIntervalMarker(n = 2));
draw(A -- B);
draw(C -- D);
draw(B -- D);
defpoint("O", intersectionpoint(line(A, C), line(B, D)), 3N + 0.5E);
markangle(B, O, A, radius = markangleradius/3);
markangle(D, O, C, radius = markangleradius/3);
draw_angle(A, C, B, repeat = 2, scale = 0.1);
draw_angle(C, A, D, repeat = 2, scale = 0.1);
