import geometry;
size(10cm);

point B = (-13, 0);
point C = (13, 0);
point A = (0, 5 * sqrt(51));

point M = (A + 1.1B)/2.1;
point K = (A + 1.1C)/2.1;

triangle abc = triangle(A, B, C);
label("$A$", A, N);
label("$B$", B, W);
label("$C$", C, E);
label("$M$", M, W);
label("$K$", K, E);

circle circ = incircle(abc);
point O = circ.C;
point H = intersectionpoint(line(B, C), altitude(abc.vertex(1)));
point Q = intersectionpoints(line(M, K), circ)[0];
point P = intersectionpoints(line(M, K), circ)[1];
point V = intersectionpoint(line(M, K), line(A, H));

label("$Q$", Q, N);
label("$P$", P, N);
label("$V$", V, SE);

draw(O -- P -- V -- cycle);

draw(abc);
draw(circ);
draw(M -- K);
fill(plain.circle(O, 0.2), black);
draw(A -- H);
label("$O$", O, W);
label("$H$", H, S);

