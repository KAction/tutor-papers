import useful;
import graph;

import graph;
size(20cm);

xaxis("$x$", RightTicks(Step=1), EndArrow);
yaxis("$y$", RightTicks(Step=1), EndArrow);

draw(graph(new real(real x) { return 1/x;}, -3, -0.3), linewidth(1.2));
draw(graph(new real(real x) { return -1/x;}, 0.3, 3), linewidth(1.2));
draw(graph(new real(real x) { return 6.25x;}, -0.5, 0.2), dashed + linewidth(1.2));
draw(graph(new real(real x) { return -6.25x;}, -0.2, 0.5), dashed + linewidth(1.2));
draw(graph(new real(real x) { return 0;}, -3, 3), dashed + linewidth(1.2));

for (real x: new real[] {0.4, -0.4}) {
  path c = shift(x, -2.5) * scale(0.08) * unitcircle;
  draw((x, 0) -- (x, -2.5) -- (0, -2.5), dotted);
  fill(c, white);
  draw(c, black);
 }
label("$\frac{2}{5}$", (0.4, 0), N);
label("$-\frac{2}{5}$", (-0.4, 0), N);
label("$-2.5$", (0, -2.5), NW);
