import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

quadrilateral tr = inquadrilateral(4, 5, 1.2, 3);
tr = rotate(-90) * tr;
unravel tr;
draw(tr);
draw(circle(A, B, C));
defpoint("K", intersectionpoint(line(A, D), line(B, C)), W);
draw(B -- K -- A -- cycle);
