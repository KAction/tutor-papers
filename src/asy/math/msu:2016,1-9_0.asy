import useful;
import geometry;
import geometry_quadrilateral;
import graph;
import _intervals;

size(10cm);

var I = toggling_intervals(plus = GrassHighlighter, in(1), out(1996));
draw(I);
