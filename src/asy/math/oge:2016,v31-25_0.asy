import useful;
import geometry;
import graph;
size(10cm);

circle c1;
circle c2;

c1.C = (0, 0);
c1.r = 1;

c2.C = (6, 0);
c2.r = 2;

draw(c1);
draw(c2);
defpoint("I", c1.C, W);
defpoint("J", c2.C, E);

line[] inner_tangents(circle c1, circle c2) {
	point O1 = c1.C;
	point O2 = c2.C;
	real r1 = c1.r;
	real r2 = c2.r;

	real d = length(segment(c1.C, c2.C));
	line[] result;
	if (r1 + r2 > d) {
		/* do nothing */
	}
	if (r1 + r2 == d) {
		point touch = (r1 * O1 + r2 * O2) / d;
		result.push(perpendicular(touch, line(O1, O2)));
	}
	if (r1 + r2 < d) {
		// WTF? rotate(real) expects degrees, not radians.
		real phi = degrees(acos((r1 + r2)/d));
		point A1 = O1 + rotate(phi)  * (r1 * unit(O2 - O1));
		point A2 = O1 + rotate(-phi) * (r1 * unit(O2 - O1));
		point B1 = O2 + rotate(phi) * (r2 * unit(O1 - O2));
		point B2 = O2 + rotate(-phi)  * (r2 * unit(O1 - O2));
		result.push(line(A1, B1));
		result.push(line(A2, B2));
		write(degrees(phi));
	}
	return result;
}

line t1 = inner_tangents(c1, c2)[1];
defpoint("A", t1.A, SE);
defpoint("B", t1.B, NW);
draw(I -- J -- B -- A -- cycle);
defpoint("K", intersectionpoint(line(A, B), line(I, J)), SE);
markrightangle(I, A, K);
markrightangle(J, B, K);
markangle(I, K, A, radius = markangleradius/1.5);
markangle(J, K, B, radius = markangleradius/1.5);
