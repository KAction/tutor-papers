import useful;
import geometry;
import graph;
size(10cm);

//scale(Linear(1), Linear(0.1));
xaxis("$x$", RightTicks(Step=1), EndArrow);
yaxis("$y$", LeftTicks(Step=1), EndArrow);

draw(graph(new real(real x) { return x*x - 12x + 27; }, 3, 9.3));
draw(graph(new real(real x) { return x*x - 4x + 3;}, 0.4, 3));

path p = (0, 0) -- (9.2, 0);
draw(p, linewidth(1.1));
label("$y = 0$", p, N + NW);

path p = (0, -1) -- (9.2, -1);
draw(p, linewidth(1.1));
label("$y = -1$", p, S + SE);
