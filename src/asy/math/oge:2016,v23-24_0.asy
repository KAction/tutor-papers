import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

quadrilateral tr = trapezium(radians(120), radians(45), h = 0.6);
draw(tr);
unravel tr;
defaltitude("H", triangle(A, C, D), 2);
defaltitude("K", triangle(A, B, C), 1);
markangle("$60\degree$", A, B, C);
markangle("$45\degree$", C, D, H, n = 2);
label("$36$", D -- C);
