import useful;
import geometry;
import geometry_quadrilateral;
import graph;
size(10cm);

quadrilateral tr = trapezium(acos(3/5), h = 0.5);
unravel tr;
draw(tr);

defaltitude("H", triangle(A, B, D).vertex(2));
defaltitude("R", triangle(A, C, D).vertex(2));
draw(segment(A, B), marker = StickIntervalMarker(n = 1, i = 1));
draw(segment(C, D), marker = StickIntervalMarker(n = 1, i = 1));
label("$r$", B -- A, 2W);
label("$r$", D -- C, 2E);
label("$a$", B -- C, N);
label("$b$", A -- D, 3S);
label("$h$", C -- R, W);
draw(A -- C -- B -- D -- cycle, dashed);
defpoint("K", intersectionpoint(line(A, C), line(B, D)), 2W);
markangle(D, A, K, n = 1, radius = markangleradius/2);
markangle(B, C, K, n = 1, radius = markangleradius/2);
markangle(A, K, D, n = 2, radius = markangleradius/3);
markangle(C, K, B, n = 2, radius = markangleradius/3);
