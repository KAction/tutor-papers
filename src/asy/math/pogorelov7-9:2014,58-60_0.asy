import useful;
import geometry;
import graph;
size(10cm);

defpoint("A", (0, 0), SW);
defpoint("B", (1, 2), NW);
defpoint("C", (4, 2), NE);
defpoint("D", (5, 0), SE);
draw(A -- B -- C -- D -- cycle);
draw(segment(A, B), marker = StickIntervalMarker(i = 1, n = 2));
draw(segment(C, D), marker = StickIntervalMarker(i = 1, n = 2));

defpoint("E", intersectionpoint(line(A, D), line(C, C + B - A)), S);
draw(C -- E);
draw(segment(C, E), marker = StickIntervalMarker(i = 1, n = 2));
markangle(E, A, B, radius = markangleradius/2);
markangle(D, E, C, radius = markangleradius/2);
markangle(C, D, E, radius = markangleradius/2);
