import useful;
import geometry;
import graph;
size(10cm);

circle c = new circle;
c.r = 1;
c.C = (0, 0);
draw(c);
defpoint("A", (2, 0), E);
defpoint("B", (1, 1), N);
defpoint("P", (-1, -2.2), W);
defpoint("Q", (1, -2.2), E);
draw(P -- Q);

circle c1 = new circle;
c1.r = length(P - Q);
c1.C = A;
draw(c1, dashed);
defpoint("C", intersectionpoints(c, c1)[0], 2S + W);
draw(A -- B -- C -- cycle, linewidth(2));
