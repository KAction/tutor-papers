import useful;
import geometry;
import geometry_quadrilateral;
import graph;

size(10cm);

real phi = radians(60);
real h = 1 / (2/tan(phi) + 1/sin(phi));
quadrilateral tr = trapezium(phi, h = h);
draw(tr);

unravel tr;
draw(A -- C);
draw(segment(A, B), marker = StickIntervalMarker(n = 2, i = 1));
draw(segment(C, B), marker = StickIntervalMarker(n = 2, i = 1));
markangle(D, A, C, n = 1);
markangle(C, A, B, n = 1);
markangle(B, C, A, n = 1);
