import useful;
import geometry;
import graph;
size(10cm);

deftriangle("ABC", triangleabc(7, 11, 13));
defpoint("M", midpoint(sg_AC), NW);
defpoint("N", midpoint(sg_CB), NE);

draw(M -- N);
defaltitude("H", triangle(A, M, N), 1);
defaltitude("K", triangle(B, N, M), 1);
defaltitude("P", triangle(C, M, N), 1);
draw(H -- K, dashed);
draw(sg_BC, marker = StickIntervalMarker(n = 2));
draw(sg_AC, marker = StickIntervalMarker(n = 1));
markangle(K, B, N, radius = 0.75 markangleradius);
markangle(P, C, N, radius = 0.75 markangleradius);
markangle(C, N, P, n = 2, radius = 0.4 markangleradius);
markangle(B, N, K, n = 2, radius = 0.4 markangleradius);
