import useful;
import geometry;
import graph;
size(10cm);

xaxis("$x$", RightTicks(Step=1), EndArrow);
yaxis("$y$", LeftTicks(Step=1), EndArrow);

draw(graph(new real(real x) { return x^2 - 4x; }, 0, 5.7));
draw(graph(new real(real x) { return -x^2 - 6x; }, -6.7, 0));
path p = (-6.5, -4) -- (5.5, -4);
draw(p, linewidth(1.1));
label("$y = -4$", (-6.5, -4) -- (0, -4));

path p = (-6.5, 9) -- (5.5, 9);
draw(p, linewidth(1.1));
label("$y = 9$", (0, 9) -- (5.5, 9), N);
