{ nixpkgs ?
  (import <nixpkgs> {}).fetchFromGitHub {
    owner = "NixOS"; # commit of 2019-02-25
    repo = "nixpkgs";
    rev = "10f0d40838cd4160ae7c7ceaaba48b0532c89887";
    sha256 = "0gcd1iqjn2y40hhgnc2p2i7g881xlgrpng5n6gs1qxcymxgkgfz4";
  }
}:
let pkgs = import nixpkgs {};
in pkgs.stdenv.mkDerivation rec {
  name = "tutor-papers-${version}";
  version = "0.1";
  src = ./.;
  buildInputs = with pkgs;
    [ tup asymptote texlive.combined.scheme-full ctags gawk cm_unicode
      fontconfig lmodern ];

  FONTCONFIG_FILE = pkgs.makeFontsConf {
    fontDirectories = [ pkgs.lmodern pkgs.cm_unicode ];
  };

  buildPhase = ''
    export HOME=$PWD
    tup init
    tup generate build.sh
    sh -eux build.sh
    '';
  installPhase = ''
    mkdir -p $out/share/tutor-papers
    cp out/*.pdf $out/share/tutor-papers
  '';
}
