#!/bin/sh
# Copyright (C) 2017 Dmitry Bogatov <KAction@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -ue
REQUIRE_USB=yes

ARGS=$(getopt -o '' -l "no-usb," -n shipout -- "$@")
eval set -- "$ARGS"
unset ARGS

while : ; do
	case "$1" in
		no-usb)
			REQUIRE_USB=no
			shift
			;;
		--)
			shift
			break
			;;
	esac
done

if [ "$REQUIRE_USB" = yes ] ; then
	pmount /dev/sdb1
	trap cleanup EXIT
	cleanup () {
		pumount /dev/sdb1
	}
	mountpoint=/media/usb0
fi

cur=0
while [ -f .git/refs/tags/print-$cur ] ; do
	: $((cur += 1))
done
prev=$((cur - 1))

list_pdfs () {
	git diff print-$prev..HEAD --numstat -- math \
		| awk '{ print $3 }' \
		| sed -r -e 's#^math/#out/#' -e 's#.tex$#.pdf#'
}
pdfs=$(list_pdfs)
unset -f list_pdfs

if [ -z "$pdfs" ] ; then
	printf "error: no new pages\n" 1>&2
	exit 1
fi

mkdir -p ~/.cache/shipout
outfile=~/.cache/shipout/$cur.pdf
pdfunite $pdfs $outfile

# Generate email to a human, managing printer.
m4 -P -Dcurrent=$cur -Doutfile="$outfile" script/m4/shipout-email.m4 \
   | refile -file /dev/stdin +drafts

if [ "$REQUIRE_USB" = yes ] ; then
	mkdir -p "$mountpoint/shipout"
	cp "$outfile" "$mountpoint/shipout/"
fi

git tag "print-$cur" HEAD
