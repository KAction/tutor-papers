\def\timestamp{unknown}
m4_ifdef(`fontsize', `\fontsize')

% `task text'
\begin{solution}
m4_ifdef(`ASY',
  `
%
  \vskip -\parskip\relax
  \vskip -0.6ex\relax
  \addpicture{l}{pictures/\thesource_0}{0.5}
%')

\end{solution}

\begin{answer}
  \inlinemath{}
\end{answer}
