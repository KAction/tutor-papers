import _range;

/* Typedef 'highlighter' is function that, well, highlight range
 * between points (x1, 0) and (x2, 0) where x1 and x2 are calculated
 * by 'ask_xpos' field of 'info' struct.
 *
 * Struct 'info' is implementation detail and is not exported.
 */
private struct info {
  real ask_xpos(limit x);
};
typedef void highlighter(range r, info i);

/* Defining operators on functions feels unnatural, but
 * it keeps things flat -- sum of two highlighters is
 * another highlighter.
 *
 * Monoid, if Asymptote were powerful enough.
 */
private void zero_highlighter(range x, info i) {};
highlighter operator+(highlighter h1, highlighter h2) {
  return new void (range r, info i) {
    h1(r, i);
    h2(r, i);
  };
}

/* Structure that represent whole real axis with point and intervals
 * marked on it. It works in write-only style: user creates instance
 * of it, call 'push' method as required and then 'draw's it.
 */
struct intervals {
  restricted range ranges[];
  restricted highlighter highlighters[];
  void push(range r, highlighter h = zero_highlighter) {
    ranges.push(r);
    highlighters.push(h);
  }
};

private typedef real ask_xpos_fn(limit l);
private ask_xpos_fn make_ask_xpos(limit arr[]) {
  return new real (limit l) {
    /* The most left point will be located at (-L, 0) and the most
     * right point will be located at (L, 0).
     */
    real L = 0.75;
    if (l.finite) {
      real ix = search(arr, l, operator <);
      return -L + ix * 2 * L/(arr.length - 1);
    } else {
      /* It actually works. */
      return l.value / infinity;
    }
  };
}

private void mark_point(limit l, info i) {
  real xpos = i.ask_xpos(l);
  path p = shift((xpos, 0)) * scale (0.013) * unitcircle;
  fill(p, l.included ? black : white);
  draw(p);
  label(l.label, (xpos, 0), SW);
}

/* Really gory details. */
void draw(intervals I, Label label = "$\scriptstyle{}x$") {
  // IMHO, arrow at right side looks better with 1.03, then with 1.0.
  // Principled solution, anyone?
  draw((-1, 0) -- (1.03, 0), EndArrow(TeXHead));
  label(label, (1.03, 0), E);
  limit points[];
  for (var r: I.ranges) {
    points.push(r.left);
    points.push(r.right);
  }
  points = filter(new bool(limit l) { return l.finite; }, points);
  points = uniq(sort(points, operator <));
  info i;
  i.ask_xpos = make_ask_xpos(points);

  for (var ix = 0; ix != I.ranges.length; ++ix)
    I.highlighters[ix](I.ranges[ix], i);
  for (var p: points)
    mark_point(p, i);

}

highlighter GrassHighlighter(bool bottom = false,
                             real slant = 1,
                             real height = 0,
                             real interval = 0) {
  /* Grass on bottom is lower, to not interfere with labels, which
     are also at bottom. */
  if (height == 0)
    height = bottom ? 0.015 : 0.03;

  if (interval == 0)
    interval = height / abs(slant);

  return new void (range r, info i) {
    real xl = i.ask_xpos(r.left);
    real xr = i.ask_xpos(r.right);

    /* Make sure that exact number of intervals fits
     * into [xl, xr]. For it, interval is adjusted a bit.
     */
    real xlength = xr - xl;
    int N = (int) (xlength / interval);
    interval = xlength / N;

    for (var ix = 0; ix != N; ++ix) {
      point src, dest;
      if (slant > 0) {
        src  = (xl + ix * interval, 0);
        dest = src + (height / slant, height);
      } else {
        src = (xr - ix *interval, 0);
        dest = src + (height / slant, abs(height));
      }
      if (bottom) {
        src  -= (0, height);
        dest -= (0, height);
      }
      draw(src -- dest);
    }
  };
}
var GrassHighlighter = GrassHighlighter();

highlighter LabelHighlighter(Label label,
                             bool bottom = false,
                             real height = 0.07)
{
  return new void (range r, info i) {
    real xl = i.ask_xpos(r.left);
    real xr = i.ask_xpos(r.right);
    real mid = (xl + xr)/2;

    point p = (mid, bottom ? -height : height);
    label(label, p);
  };
}
var PlusHighlighter = LabelHighlighter("$+$");
var MinusHighlighter = LabelHighlighter("$-$");

highlighter ArrowHighlighter(bool bottom = true,
                             bool increase = true,
                             real narrow = 0.8,
                             real low = 0.07,
                             real high = 2 * low)
{
  return new void (range r, info i) {
    real xl = i.ask_xpos(r.left);
    real xr = i.ask_xpos(r.right);
    real len = xr - xl;
    real xL = xl + (1 - narrow) / 2 * len;
    real xR = xr - (1 - narrow) / 2 * len;
    real yL, yR;

    if (increase) {
      yL = low;
      yR = high;
    } else {
      yL = high;
      yR = low;
    }

    if (bottom) {
      real tmp = yL;
      yL = -yR;
      yR = -tmp;
    }
    draw((xL, yL) -- (xR, yR), EndArrow(TeXHead));
  };
}
var IncreaseHighlighter = ArrowHighlighter(increase = true);
var DecreaseHighlighter = ArrowHighlighter(increase = false);

intervals toggling_intervals(intervals I = null,
                             highlighter plus = zero_highlighter,
                             highlighter minus = zero_highlighter
                             ...limit[] points)
{
  if (I == null)
    I = new intervals;

  plus = plus + PlusHighlighter;
  minus = minus + MinusHighlighter;
  highlighter hs[] = { plus, minus };
  hs.cyclic = true;

  points.insert(0, -infinity);
  points.push(infinity);

  var len = points.length;
  for (var ix = 0; ix != len - 1; ++ix) {
    var left = points[len - ix - 2];
    var right = points[len - ix - 1];
    I.push(left -- right, hs[ix]);
  };

  return I;
}
