// useful.asy --- ubiquity 'utitlty' module for geometry modules

// Copyright (C) 2016 Dmitry Bogatov <KAction@gnu.org>

// Author: Dmitry Bogatov <KAction@gnu.org>

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

import three;
import geometry;
import graph;

usepackage("gensymb"); // Enables \degree command in labels
/* = Casts =
 *
 * To make use of 'sprintf' function more convenient, we define number implicit
 * casts to string. Wish it will not end in Perl-like mess.
 */
private string operator cast(point p) { return (string) (pair) p; }

/* = String library =
 *
 * Asymptote string manipulation support in standard library is rather
 * cumbersome. While provided functions are sufficent to implement any
 * string transformation, they are not very convenient to do so.
 */


// Strings are not arrays, and there is no way to make them support
// indexing syntax, but at least we can define function to access
// individual characters.

string at(string s, int i)
{
	return substr(s, i, 1);
}

// There is standard function 'format', but it is rather limited.
// Here is 'sprintf', Python-like formatting function, that can be
// invoked like this:
//
// string s = sprintf('{0} --> {1}:{0}', "foo", "bar")
//
// It is necessary to implement two versions -- one, which accepts
// array of substitution strings, and one, which accepts rest
// arguments, which is more convenient to use, but is not composable,
// since there is no 'apply' function in Asymptote. As far, as I know.
string sprintf(string fmt, string[] subst) {
	string[][] vars = {};
	for (int i = 0; i != subst.length; ++i) {
		string token = "{" + (string) i + "}";
		vars.push(new string [] { token, subst[i] });
	}
	return replace(fmt, vars);
}

string sprintf(string fmt ... string[] subst) {
	return sprintf(fmt, subst);
}

/* = Creating triangles =
 *
 * Standard Asymptote module geometry provides many constructors for
 * triangle structure. Here we are defining some more.
 *
 * Unlike geometry module itself, I prefer functions to have manageble
 * amount of arguments. So there is no 'rotate' argument or something
 * like this, since it is possible to transform triangles by standard
 * means:
 *
 * triangle T = // make triangle somehow
 * triangle T2 = scale(4) * T;
 */

// Return triangle ABC with B = (-0.5, 0), C = (0.5, 0) and angles B
// and C equal to 'beta' and 'gamma' correspondingly (in radians, obviously).
// In is guaranteed, that y-coordinate of point A is positive.
//
// Unfortunately, it is hard to explain in comments, why coordinates
// of point A calculated exactly this way. There is TeX documentation
// in abandoned meta/source.nw file.
triangle triangle(real beta, real gamma = pi/2)
{
	real tG = tan(gamma);
	real tB = tan(beta);
	real tSum = tG + tB;
	real x = 0.5 * (tG - tB)/tSum;
	real y = tG * tB/ tSum;
	point B = (-0.5, 0);
	point C = (0.5, 0);
	point A = (x, y);
	return triangle(A, B, C);
}


/* = Defining objects =
 *
 * And here comes 'eval' insanity. But firstly, motivating example.
 *
 * point A = (3, 4);
 * label("$A$", A, NW);
 *
 * It is extremely common pattern, and duplication is obvious -- label
 * text can be inferred from from variable name. Or, since Asymptote
 * has 'eval', but not proper ( = Lisp) macros, variable name can be
 * inferred from label. So here comes 'defpoint' function.
 */


// It is rather common coding patter in this section -- format string
// and eval it. Function 'evalf' provides a shortcut.
void evalf(string fmt ... string[] subst) {
	string l = sprintf(fmt, subst);
	// write(l);
	eval(l, true);
}

// Define variable named 'name' of type 'pair' with specified 'value'
// and label it with its name. Align label according to 'align' function
// argument.
void defpoint(string name, pair value, pair align = (0, 0)) {
	evalf('pair {0} = {1}', name, (string) value);
	evalf('label("${0}$", {1}, {2})', name, (string) value, (string) align);
}

// Given point p1 named n1 and point p2 named n2 define variables
// sg_<n1><n2> and sg_<n2><n1>, both are segment(p1, p2)
private void define_segment(string n1, point p1, string n2, point p2) {
	// Difference in order of {0} and {1}, so both sg_AB and sg_BA
	// are defined.
	evalf('segment sg_{0}{1} = segment({2}, {3})', n1, n2, p1, p2);
	evalf('segment sg_{1}{0} = segment({2}, {3})', n1, n2, p1, p2);
}

// Define point of given 'name' and value P with alignment, dictated
// the fact, that P, R1 and R2 are vertexes of drawn triangle.
private void define_vertex(point P, point R1, point R2, string name) {
	// Align in direction away from other triangle points
	pair align = unit(unit(P - R1) + unit(P - R2));
	defpoint(name, P, align);
}

// For triangle with vertexes, located in points p0, p1, p2 and named
// according to 'letters' argument, perform following actions:
//
//  - define 'point' variables of vertexes and label them. Variables
//    are created without prefix (deprecated) and with 'p' prefix.
//    See 'defpoint'
//
//  - define 'segment' variables of sides with 'sg_' prefix
//
//  - define 'triangle' variable without prefix
//
//  - define 'vertex' variables for vertexes with 'v' prefix
//
// For purposes of backward compatibility optional draw parameter defines,
// whether triangle itself should be drawn. It may be useful if you want
// to draw triangle yourself with custom pen.
void deftriangle(string letters, point p0, point p1, point p2, bool draw = true)
{
	string letter (int i) { return at(letters, i); }

	define_vertex(p0, p1, p2, letter(0));
	define_vertex(p1, p2, p0, letter(1));
	define_vertex(p2, p0, p1, letter(2));

	define_segment(letter(0), p0, letter(1), p1);
	define_segment(letter(0), p0, letter(2), p2);
	define_segment(letter(1), p1, letter(2), p2);

	evalf('triangle {3} = triangle({0}, {1}, {2})', p0, p1, p2, letters);

	for (int i = 0; i != 3; ++i)
		evalf('vertex v{0} = {1}.vertex({2})',
		      letter(i), letters, (string) i);
	if (draw)
		evalf('draw({0})', letters);
}

// If you have already constructed triangle, this overloaded version
// will dispatch it into three points for you.
void
deftriangle (string letters, triangle t)
{
	deftriangle(letters, t.point(1), t.point(2), t.point(3));
}


// While geometry provides 'altitude' function, in returns 'line'.
// This function fulfills common need, not covered by geometry module:
//
//  - define basepoint of altitude
//  - draw altitude exactly from vertex to opposite side
//  - mark right angle

void defaltitude(string var, vertex v)
{
	unravel v;
	line altitude = altitude(v);
	line aganist = t.side(n + 1 % 3);
	pair align = unit(altitude.B - altitude.A);
	point p = intersectionpoint(altitude, aganist);

	defpoint(var, p, align);
	draw(t.point(n) -- p);
	markrightangle(t.point(n), p, t.point(n + 1 % 3));
}

// Deprecated.
void defaltitude(string var, triangle t, int n)
{
	defaltitude(var, t.vertex(n));
}


/* = Inequations =
 *
 * Solving inequations requires drawing axis, putting points on it and
 * drawing visual clues, which segments intersect. It worth to write
 * proper solution, that would do it automatically, but for now, all
 * I have is simple helper function.
 */

void mark_point(real value, Label l = null, bool exclude = false) {
	l = (l != null) ? l : Label(string(value));
	path c = circle((value, 0), 0.05);
	pen p = exclude ? white : black;
	fill(c, p);
	draw(c);
	label(l, (value, 0), 2S);
}

/* = Three =
 *
 * Unlike their counterparts for 2D images, I am now aware of more
 * advanced alternatives to them, so these functions are not
 * deprecated.
 *
 * But they still very inflexible, comparing with 'markangle' from
 * geometry module.
 */

path3 angle(triple c, triple a, triple b, real scale = 0.2) {
	real len = scale * min(length(a - c), length(b - c));
	return arc(c, c + unit(a - c) * len, c + unit(b - c) * len);
}

path3 orth(triple c, triple a, triple b, real scale = 0.2) {
	real len = scale * min(length(a - c), length(b - c));
	triple u1 = len * unit(a - c);
	triple u2 = len * unit(b - c);
	return c + u1 -- c + u1 + u2 -- c + u2;
}

/* = Deprecated =
 *
 * All functions below are deprecated. They were created due my poor
 * knowledge and understanding of Asymptote standard modules. I expect
 * that this section will expand due increase in my wisdom.
 *
 * Ideally, at all call sites, this functions should be replaces by
 * better alternative, but it is not very urgent task.
 */

// Following functions are superseded by 'markangle' and 'markrightangle'
// from geometry module.
path orth(pair c, pair a, pair b, real scale = 0.2) {
	real len = scale * min(length(a - c), length(b - c));
	pair u1 = len * unit(a - c);
	pair u2 = len * unit(b - c);
	return c + u1 -- c + u1 + u2 -- c + u2;
}

path angle(pair c, pair a, pair b, real scale = 0.2) {
	real len = scale * min(length(a - c), length(b - c));
	return arc(c, c + unit(a - c) * len, c + unit(b - c) * len);
}

void draw_angle(pair c, pair a, pair b, real scale = 0.2, int repeat = 1)
{
	for (int i = 0; i != repeat; ++i) {
		draw(angle(c, a, b, scale));
		scale *= 1.15;
	}
}

// Local Variables:
// eval: (c-set-style "linux")
// End:
