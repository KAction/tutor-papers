T[] uniq(T arr[], bool eq(T, T) = operator ==) {
  T result[];
  T last = arr[0];
  result.push(arr[0]);
  for (var x: arr[1:]) {
    if (x != last) {
      result.push(x);
      last = x;
    }
  }
  return result;
}
