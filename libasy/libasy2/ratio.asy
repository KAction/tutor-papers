// Copyright (C) 2017 Dmitry Bogatov
//
// This file is part of libasy2.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

access "libasy2/string.asy" as str;
unravel str;

private int gcd(int a, int b)
{
	while (b != 0) {
		int c = a;
		a = b;
		b = c % a;
	}
	return a;
}

private int signum(int a)
{
	if (a > 0)
		return 1;
	if (a < 0)
		return -1;
	return 0;
}

struct ratio {
	restricted int numerator;
	restricted int denominator;
	void operator init(int num, int denom) {
		int sign = signum(num) * signum(denom);
		int k = gcd(abs(num), abs(denom));
		this.numerator = sign * abs(num) # k;
		this.denominator = abs(denom) # k;
	};
};

ratio operator:: (int n, int m)
{
	return ratio(n, m);
}

ratio operator cast(int n)
{
	return ratio(n, 1);
}

ratio operator- (ratio r)
{
	return ratio(-r.numerator, r.denominator);
}

ratio operator+ (ratio r1, ratio r2)
{
	return ratio(r1.numerator * r2.denominator + r2.numerator * r1.denominator,
		     r1.denominator * r2.denominator);
}

ratio operator- (ratio r1, ratio r2)
{
	return r1 + (-r2);
}

ratio operator* (ratio r1, ratio r2)
{
	return ratio(r1.numerator * r2.numerator,
		     r1.denominator * r2.denominator);
}

ratio operator/ (ratio r1, ratio r2)
{
	return ratio(r1.numerator * r2.denominator, r1.denominator * r2.numerator);
}

ratio operator:: (ratio r1, ratio r2)
{
	return r1 / r2;
}

private ratio inverse(ratio r)
{
	unravel r;
	return ratio(denominator, numerator);
}

ratio operator^ (ratio r, int n)
{
	if (n < 0)
		return (inverse(r)) ^ (-n);
	ratio result = 1;
	for (var i = 0; i != n; ++i)
		result *= r;
	return result;
}

bool operator ==(ratio r1, ratio r2)
{
	var diff = r1 - r2;
	return diff.numerator == 0;
}

bool operator <=(ratio r1, ratio r2)
{
	var diff = r1 - r2;
	return diff.numerator <= 0;
}

bool operator!= (ratio r1, ratio r2) { return !(r1 == r2); }
bool operator<  (ratio r1, ratio r2) { return (r1 <= r2) && (r1 != r2); }
bool operator>= (ratio r1, ratio r2) { return !(r1 < r2); }
bool operator>  (ratio r1, ratio r2) { return !(r1 <= r2); }

void write(ratio r)
{
	write(str.sprintf("{0}/{1}",
			  (string) r.numerator,
			  (string) r.denominator));
}
