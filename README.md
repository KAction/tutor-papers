[![pipeline status](https://gitlab.com/KAction/tutor-papers/badges/master/pipeline.svg)](https://gitlab.com/KAction/tutor-papers/commits/master)

This repository contains sources of my collection of solutions to math
tasks, suitable to be shown to students as perfect solution. Every
solution is single A5 page.

Unless you read Russian, I doubt it would be of much use for you; TeX
code, while documented in English, if far from being exemplar.
